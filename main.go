package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/cobra"
)

type Worker interface {
	Start(context.Context) error
	Stop()
}

type APIServer struct {
	listenAddr string
	ctx        context.Context
	cancel     func()
	handle     func(*net.UnixConn)
	died       chan struct{}
}

type accept struct {
	conn *net.UnixConn
	err  error
}

func (a *APIServer) Start(ctx context.Context) error {
	newCtx, cancelFunc := context.WithCancel(ctx)
	a.ctx = newCtx
	a.cancel = cancelFunc

	// networks supported are unix, unixgram and unixpacket
	addr, err := net.ResolveUnixAddr("unix", a.listenAddr)
	if err != nil {
		return err
	}

	listener, err := net.ListenUnix("unix", addr)
	if err != nil {
		return err
	}

	ach := make(chan accept)

eventloop:
	for {
		log.Println("start iter")
		go func() {
			conn, err := listener.AcceptUnix()
			ach <- accept{conn, err}
		}()

		log.Println("mid iter")
		select {
		case <-a.ctx.Done():
			log.Println("stopping service")
			break eventloop
		case ac := <-ach:
			if ac.err != nil {
				log.Println(ac.err)
			} else {
				go a.handle(ac.conn)
			}
		}

		log.Println("iter")
	}

	select {
	case a.died <- struct{}{}:
	default:
	}
	return nil
}

func (a *APIServer) Stop() {
	a.cancel()
	<-a.died
}

func NewServer(addr string, handler interface{}) *APIServer {
	return &APIServer{
		listenAddr: addr,
		died:       make(chan struct{}, 1),
		handle: func(c *net.UnixConn) {
			go func() {
				for {
					buff := make([]byte, 2048)
					n, _, err := c.ReadFromUnix(buff)
					if err != nil && err != io.EOF {
						log.Println("closing conn::", err.Error())
						return
					}

					buff = buff[:n]
					fmt.Println("got", string(buff))
				}
			}()
		},
	}
}

func main() {
	var socketAddress string

	cmd := &cobra.Command{
		Use:   "stackmd",
		Short: "the p2p internet stack",
		Run: func(command *cobra.Command, args []string) {
			log.Println("starting daemon")

			// move networking to machapi package
			mach := NewServer("@machine.api.stackmesh.io", nil)
			go func() {
				err := mach.Start(context.Background())
				if err != nil {
					fmt.Println(err.Error())
					os.Exit(1)
				}
			}()

			kill := make(chan os.Signal, 1)
			signal.Notify(kill, syscall.SIGINT, syscall.SIGTERM)

			// block until exit
			<-kill
			mach.Stop()
		},
	}

	cmd.Flags().StringVar(&socketAddress, "socket-addr", "@machine.api.stackmesh.io", "machine api socket location")

	err := cmd.Execute()
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
